# Testing the plugin

## End to end

Prepare a python environment to execute the E2E tests suite

```shell
python3 -m venv venv
source venv/bin/activate
pip install -r tests/requirements.e2e.txt
python -m unittest discover tests/e2e
```
