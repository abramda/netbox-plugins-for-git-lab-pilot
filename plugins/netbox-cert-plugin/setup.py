from setuptools import find_packages, setup

setup(
    name='netbox_cert_plugin',
    version='0.0.4',
    description='A Netbox plugin to manage certificates',
    install_requires=[],
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False
)
