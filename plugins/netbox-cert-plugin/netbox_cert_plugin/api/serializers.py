"""API Serializer definitions"""

from rest_framework import serializers
from netbox.api.serializers import NetBoxModelSerializer, WritableNestedSerializer
from ..models import Certificate


class NestedCertificateSerializer(WritableNestedSerializer):
    """Nested Certificate Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_cert_plugin-api:certificate-detail"
    )

    class Meta:
        model = Certificate
        fields = (
            "id",
            "url",
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "cert_created_at",
            "cert_expired_at",
            "state",
            "custom_fields",
            "created",
            "last_updated",
            "tags",
        )


class CertificateSerializer(NetBoxModelSerializer):
    """Certificate Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_cert_plugin-api:certificate-detail"
    )

    class Meta:
        model = Certificate
        fields = (
            "id",
            "url",
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "cert_created_at",
            "cert_expired_at",
            "state",
            "custom_fields",
            "created",
            "last_updated",
            "tags",
        )
