"""API URLs definition"""

from netbox.api.routers import NetBoxRouter
from . import views

APP_NAME = 'netbox_cert_plugin'

router = NetBoxRouter()
router.register('certificate', views.CertificateViewSet)

urlpatterns = router.urls
