"""Forms definitions"""

from django import forms
from netbox.forms import (
    NetBoxModelForm,
    NetBoxModelImportForm,
    NetBoxModelFilterSetForm,
)
from utilities.forms.widgets import DatePicker
from utilities.forms.fields import TagFilterField
from .models import Certificate, ExpirationTimeChoices, CaChoices, StateChoices


class CertificateForm(NetBoxModelForm):
    """Certificate form definition class"""

    class Meta:
        model = Certificate
        fields = (
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "state",
            "cert_created_at",
            "cert_expired_at",
            "tags",
        )
        help_texts = {"CN": "Unique Common Name", "CA": "Valid Certificate Authority"}
        labels = {
            "cn": "CN",
            "alt_name": "Alt name",
            "ca": "CA",
            "expiration_time": "Expiration time",
            "state": "Certificate state",
            "cert_created_at": "Effective certificate's creation date",
            "cert_expired_at": "Effective certificate's expiration date",
        }
        widgets = {
            "cert_created_at": DatePicker(),
            "cert_expired_at": DatePicker(),
        }


class CertificateFilterForm(NetBoxModelFilterSetForm):
    """Certificate filter form definition class"""

    model = Certificate
    cn = forms.CharField(
        label="Common Name", max_length=256, min_length=1, required=False
    )
    alt_name = forms.CharField(
        label="Alt name", max_length=256, min_length=1, required=False
    )
    ca = forms.MultipleChoiceField(
        label="Certificate Authority", choices=CaChoices, required=False
    )
    expiration_time = forms.MultipleChoiceField(
        label="Expiration time", choices=ExpirationTimeChoices, required=False
    )
    state = forms.MultipleChoiceField(
        label="Certificate state", choices=StateChoices, required=False
    )
    cert_created_at = forms.DateField(
        label="Effective certificate's creation date", required=False, widget=DatePicker
    )
    cert_expired_at = forms.DateField(
        label="Effective certificate's expirations date",
        required=False,
        widget=DatePicker,
    )
    tag = TagFilterField(model)


class CertificateImportForm(NetBoxModelImportForm):
    """Certificate importation form definition class"""

    class Meta:
        model = Certificate
        fields = (
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "state",
            "cert_created_at",
            "cert_expired_at",
        )
        labels = {
            "ca": "Certificate Authority. Can be 'letsencrypt', 'comisign', 'globalsign'",
            "expiration_time": "Expiration time needed. Can be '1m', '3m', '6m', '1y', '3y'",
            "state": "Certificate status. Can be 'pending', 'valid', 'to_be_renewed'",
            "alt_name": 'Alt name separated by commas, encased with double quotes (e.g. "alt1,alt2,alt3")',
        }
