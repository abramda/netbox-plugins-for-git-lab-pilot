"""Navigation Menu definitions"""

from extras.plugins import PluginMenuButton, PluginMenuItem, PluginMenu
from utilities.choices import ButtonColorChoices

cert_buttons = [
    PluginMenuButton(
        link="plugins:netbox_cert_plugin:certificate_add",
        title="Add",
        icon_class="mdi mdi-plus-thick",
        color=ButtonColorChoices.GREEN,
    ),
    PluginMenuButton(
        link="plugins:netbox_cert_plugin:certificate_import",
        title="Import",
        icon_class="mdi mdi-upload",
        color=ButtonColorChoices.CYAN,
    ),
]

certItem = [
    PluginMenuItem(
        link="plugins:netbox_cert_plugin:certificate_list",
        link_text="Certificates",
        buttons=cert_buttons,
        permissions=["netbox_cert_plugin.view_certificate"],
    ),
]

menu = PluginMenu(
    label="Certificates",
    groups=(("CERTIFICATE", certItem),),
    icon_class="mdi mdi-certificate",
)
