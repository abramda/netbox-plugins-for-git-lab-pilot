"""Netbox Plugin Configuration"""

# pylint: disable=E0401
from extras.plugins import PluginConfig


class NetBoxCertConfig(PluginConfig):
    """Netbox Plugin Configuration class"""

    name = "netbox_cert_plugin"
    verbose_name = "Netbox Certificate"
    description = "A Netbox plugin to manage certificates"
    version = "0.0.4"
    author = "Vincent Simonin"
    author_email = "vincent.simonin@ext.ec.europa.eu"
    base_url = "cert"

# pylint: disable=C0103
config = NetBoxCertConfig
