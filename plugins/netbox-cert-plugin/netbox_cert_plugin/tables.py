"""Tables definitions"""

import django_tables2 as tables
from netbox.tables import NetBoxTable, columns
from .models import Certificate


class CertificateTable(NetBoxTable):
    """Certificate Table definition class"""

    cn = tables.Column(linkify=True)
    tags = columns.TagColumn()

    class Meta(NetBoxTable.Meta):
        model = Certificate
        fields = (
            "pk",
            "id",
            "cn",
            "alt_name",
            "ca",
            "state",
            "expiration_time",
            "cert_created_at",
            "cert_expired_at",
            "tags",
        )
        default_columns = (
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "state",
            "cert_created_at",
            "cert_expired_at",
        )
