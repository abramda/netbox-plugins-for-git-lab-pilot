"""Filtersets definitions"""

from django_filters import filters
from netbox.filtersets import NetBoxModelFilterSet
from django.db.models import Q
from .models import Certificate


class CertificateFilterSet(NetBoxModelFilterSet):
    """Certificate filterset definition class"""

    alt_name = filters.CharFilter(lookup_expr="icontains")

    class Meta:
        model = Certificate
        fields = (
            "id",
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "state",
            "cert_created_at",
            "cert_expired_at",
        )

    # pylint: disable=W0613
    def search(self, queryset, name, value):
        """override"""
        if not value.strip():
            return queryset
        return queryset.filter(Q(cn__icontains=value) | Q(alt_name__icontains=value))
