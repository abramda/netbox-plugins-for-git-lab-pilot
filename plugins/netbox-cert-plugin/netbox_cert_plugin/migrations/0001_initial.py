"""Migration File"""
# pylint: disable=C0103

from django.db import migrations, models
import taggit.managers
import utilities.json


class Migration(migrations.Migration):
    """Migration Class"""

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Certificate",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True, primary_key=True, serialize=False
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True, null=True)),
                ("last_updated", models.DateTimeField(auto_now=True, null=True)),
                (
                    "custom_field_data",
                    models.JSONField(
                        blank=True,
                        default=dict,
                        encoder=utilities.json.CustomFieldJSONEncoder,
                    ),
                ),
                ("cn", models.CharField(blank=False, unique=True, max_length=256)),
                ("alt_name", models.CharField(blank=True, null=True, max_length=256)),
                ("ca", models.CharField(blank=False, max_length=32)),
                ("expiration_time", models.CharField(blank=False, max_length=2)),
                ("cert_created_at", models.DateField(blank=True, null=True)),
                ("cert_expired_at", models.DateField(blank=True, null=True)),
                (
                    "tags",
                    taggit.managers.TaggableManager(
                        through="extras.TaggedItem", to="extras.Tag"
                    ),
                ),
            ],
            options={
                "ordering": ("source"),
            },
        ),
    ]
