"""Migration File"""
# pylint: disable=C0103

from django.db import migrations, models


class Migration(migrations.Migration):
    """Migration Class"""

    dependencies = [
        ("netbox_cert_plugin", "0002_new_alt_name_array"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="certificate",
            options={"ordering": ["cn"]},
        ),
        migrations.AddField(
            model_name="certificate",
            name="state",
            field=models.CharField(default="valid", max_length=32),
        ),
        migrations.AlterField(
            model_name="certificate",
            name="ca",
            field=models.CharField(default="letsencrypt", max_length=32),
        ),
        migrations.AlterField(
            model_name="certificate",
            name="expiration_time",
            field=models.CharField(default="1m", max_length=2),
        ),
    ]
