"""Search definitions"""

from netbox.search import SearchIndex, register_search
from .models import Certificate


@register_search
class CertificateIndex(SearchIndex):
    """Certificate search definition class"""

    model = Certificate
    fields = (("cn", 256), ("alt_name", 256), ("ca", 32), ("state", 32))
