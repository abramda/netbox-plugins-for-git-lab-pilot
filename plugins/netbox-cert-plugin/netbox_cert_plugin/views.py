"""Model views definitions"""

from netbox.views import generic
from netbox_cert_plugin import models, tables, filtersets, forms
from django.utils.translation import gettext as _


class CertificateView(generic.ObjectView):
    """Certificate view definition"""

    queryset = models.Certificate.objects.all()


class CertificateListView(generic.ObjectListView):
    """Certificate list view definition"""

    queryset = models.Certificate.objects.all()
    table = tables.CertificateTable
    filterset = filtersets.CertificateFilterSet
    filterset_form = forms.CertificateFilterForm


class CertificateEditView(generic.ObjectEditView):
    """Certificate edition view definition"""

    queryset = models.Certificate.objects.all()
    form = forms.CertificateForm


class CertificateBulkImportView(generic.BulkImportView):
    """Certificate bulk import view definition"""

    queryset = models.Certificate.objects.all()
    model_form = forms.CertificateImportForm


class CertificateDeleteView(generic.ObjectDeleteView):
    """Certificate delete view definition"""

    queryset = models.Certificate.objects.all()


class CertificateBulkDeleteView(generic.BulkDeleteView):
    """Certificate bulk delete view definition"""

    queryset = models.Certificate.objects.all()
    filterset = filtersets.CertificateFilterSet
    table = tables.CertificateTable
