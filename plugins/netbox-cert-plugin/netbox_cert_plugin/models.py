"""Models definitions"""

from django.db import models
from django.urls import reverse
from django.contrib.postgres.fields.array import ArrayField
from netbox.models import NetBoxModel
from utilities.choices import ChoiceSet


class CaChoices(ChoiceSet):
    """CA choices definition class"""

    key = "Certificate.ca"

    DEFAULT_VALUE = "letsencrypt"

    CHOICES = [
        ("letsencrypt", "Let's Encrypt", "yellow"),
        ("commissign", "CommisSign", "red"),
        ("globalsign", "GlobalSign", "blue"),
    ]


class ExpirationTimeChoices(ChoiceSet):
    """Expiration time choices definition class"""

    key = "Certificate.expiration_time"

    DEFAULT_VALUE = "1m"

    CHOICES = [
        ("1m", "1 month", "blue"),
        ("3m", "3 months", "blue"),
        ("6m", "6 months", "blue"),
        ("1y", "1 year", "blue"),
        ("3y", "3 year", "blue"),
    ]


class StateChoices(ChoiceSet):
    """CA choices definition class"""

    key = "Certificate.state"

    DEFAULT_VALUE = "to_be_created"

    CHOICES = [
        ("to_be_created", "To be created", "yellow"),
        ("creating", "Creating", "green"),
        ("errored", "Errored", "red"),
        ("valid", "Valid", "blue"),
        ("to_be_renewed", "To be renewed", "black"),
        ("renewing", "Renewing", "green"),
    ]


class Certificate(NetBoxModel):
    """Certificate definition class"""

    cn = models.CharField(
        max_length=256,
        blank=False,
        verbose_name="Common Name",
        unique=True,
        help_text="Unique Common Name",
    )
    alt_name = ArrayField(
        base_field=models.CharField(max_length=256),
        null=True,
        blank=True,
        verbose_name="Alt Name",
        help_text="Alt Name is a list of host separated by commas (e.g. alt1,alt2,alt3)",
    )
    ca = models.CharField(
        max_length=32,
        choices=CaChoices,
        default=CaChoices.DEFAULT_VALUE,
        blank=False,
        verbose_name="Certificate Authority",
    )
    expiration_time = models.CharField(
        max_length=2,
        choices=ExpirationTimeChoices,
        default=ExpirationTimeChoices.DEFAULT_VALUE,
        blank=False,
        verbose_name="Expiration time needed",
    )
    cert_created_at = models.DateField(
        blank=True, null=True, verbose_name="Effective certificate's creation date"
    )
    cert_expired_at = models.DateField(
        blank=True, null=True, verbose_name="Effective certificate's expiration date"
    )
    state = models.CharField(
        max_length=32,
        choices=StateChoices,
        default=StateChoices.DEFAULT_VALUE,
        blank=False,
        verbose_name="Certificate State",
    )

    class Meta:
        ordering = ["cn"]

    def __str__(self):
        return f"{self.cn}"

    def get_absolute_url(self):
        """override"""
        return reverse("plugins:netbox_cert_plugin:certificate", args=[self.pk])
