"""URL definitions"""

from django.urls import path
from netbox_rps_plugin import views, models
from netbox.views.generic import ObjectChangeLogView, ObjectJournalView


urlpatterns = (

    # Mapping
    path('mappings/', views.MappingListView.as_view(), name='mapping_list'),
    path('mappings/add/', views.MappingEditView.as_view(), name='mapping_add'),
    path('mappings/import/', views.MappingBulkImportView.as_view(), name='mapping_import'),
    path('mappings/delete/', views.MappingBulkDeleteView.as_view(), name='mapping_bulk_delete'),
    path('mappings/<int:pk>/', views.MappingView.as_view(), name='mapping'),
    path('mappings/<int:pk>/edit/', views.MappingEditView.as_view(), name='mapping_edit'),
    path('mappings/<int:pk>/delete/', views.MappingDeleteView.as_view(), name='mapping_delete'),
    path('mappings/<int:pk>/http-headers/', views.MappingHttpHeadersView.as_view(), name='mapping_httpheader'),
    path('mappings/<int:pk>/saml-config/', views.MappingSamlConfigView.as_view(), name='mapping_samlconfig'),
    path('mappings/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='mapping_changelog', kwargs={
        'model': models.Mapping
    }),
    path('mappings/<int:pk>/journal/', ObjectJournalView.as_view(), name='mapping_journal', kwargs={
        'model': models.Mapping
    }),

    # HTTP Headers
    path('http-headers/', views.HttpHeaderListView.as_view(), name='httpheader_list'),
    path('http-headers/add/', views.HttpHeaderEditView.as_view(), name='httpheader_add'),
    path('http-headers/delete/', views.HttpHeaderBulkDeleteView.as_view(), name='httpheaders_bulk_delete'),
    path('http-headers/<int:pk>/', views.HttpHeaderView.as_view(), name='httpheader'),
    path('http-headers/<int:pk>/edit/', views.HttpHeaderEditView.as_view(), name='httpheader_edit'),
    path('http-headers/<int:pk>/delete/', views.HttpHeaderDeleteView.as_view(), name='httpheader_delete'),
    path('http-headers/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='httpheader_changelog', kwargs={
        'model': models.HttpHeader
    }),
    path('http-headers/<int:pk>/journal/', ObjectJournalView.as_view(), name='httpheader_journal', kwargs={
        'model': models.HttpHeader
    }),

    # Saml Config
    path('saml-configs/add/', views.SamlConfigEditView.as_view(), name='samlconfig_add'),
    path('saml-configs/<int:pk>/edit/', views.SamlConfigEditView.as_view(), name='samlconfig_edit'),
    path('saml-configs/<int:pk>/delete/', views.SamlConfigDeleteView.as_view(), name='samlconfig_delete'),
)
