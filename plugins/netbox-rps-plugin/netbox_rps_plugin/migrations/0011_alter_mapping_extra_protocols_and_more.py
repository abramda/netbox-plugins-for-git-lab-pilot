"""Migration File"""
# pylint: disable=C0103

import django.contrib.postgres.fields
from django.db import migrations, models
import netbox_rps_plugin.models


class Migration(migrations.Migration):
    """Migration Class"""

    dependencies = [
        (
            "netbox_rps_plugin",
            "0010_alter_httpheader_options_alter_mapping_options_and_more",
        ),
    ]

    operations = [
        migrations.AlterField(
            model_name="mapping",
            name="extra_protocols",
            field=django.contrib.postgres.fields.ArrayField(
                base_field=models.CharField(max_length=32),
                blank=True,
                default=netbox_rps_plugin.models.default_protocol,
                size=None,
            ),
        ),
        migrations.AddConstraint(
            model_name="mapping",
            constraint=models.CheckConstraint(
                check=models.Q(("source__exact", models.F("target")), _negated=True),
                name="netbox_rps_plugin_mapping_check_target_source_url",
            ),
        ),
    ]
