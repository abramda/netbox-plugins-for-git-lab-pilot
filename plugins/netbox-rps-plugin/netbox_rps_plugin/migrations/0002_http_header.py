"""Migration File"""
# pylint: disable=C0103

import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion
import taggit.managers
import utilities.json


class Migration(migrations.Migration):
    """Migration Class"""

    initial = True

    dependencies = [("netbox_rps_plugin", "0001_initial")]

    operations = [
        migrations.CreateModel(
            name="HttpHeader",
            # pylint: disable=R0801
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True, primary_key=True, serialize=False
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True, null=True)),
                ("last_updated", models.DateTimeField(auto_now=True, null=True)),
                (
                    "custom_field_data",
                    models.JSONField(
                        blank=True,
                        default=dict,
                        encoder=utilities.json.CustomFieldJSONEncoder,
                    ),
                ),
                ("name", models.CharField(blank=True, max_length=120)),
                ("value", models.CharField(blank=True, max_length=120)),
                ("apply_to", models.CharField(max_length=30)),
                (
                    "mapping",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="http_headers",
                        to="netbox_rps_plugin.mapping",
                    ),
                ),
                (
                    "tags",
                    taggit.managers.TaggableManager(
                        through="extras.TaggedItem", to="extras.Tag"
                    ),
                ),
            ],
            options={
                "ordering": ("name"),
            },
        ),
    ]
