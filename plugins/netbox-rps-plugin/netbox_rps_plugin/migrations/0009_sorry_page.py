"""Migration File"""
# pylint: disable=C0103

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    """Migration Class"""

    dependencies = [
        ("netbox_rps_plugin", "0008_new_mapping_attributes"),
    ]

    operations = [
        migrations.AddField(
            model_name="mapping",
            name="sorry_page",
            field=models.CharField(
                default=settings.PLUGINS_CONFIG["netbox_rps_plugin"]["default_sorry_page"],
                max_length=2000,
                validators=[
                    django.core.validators.URLValidator(message="It must be a url")
                ],
            ),
        ),
    ]
