"""Migration File"""
# pylint: disable=C0103

from django.db import migrations, models
import utilities.json
import taggit.managers
import django.db.models.deletion


class Migration(migrations.Migration):
    """Migration Class"""

    dependencies = [("netbox_rps_plugin", "0006_url_max_length")]

    operations = [
        migrations.CreateModel(
            name="SamlConfig",
            # pylint: disable=R0801
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True, primary_key=True, serialize=False
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True, null=True)),
                ("last_updated", models.DateTimeField(auto_now=True, null=True)),
                (
                    "custom_field_data",
                    models.JSONField(
                        blank=True,
                        default=dict,
                        encoder=utilities.json.CustomFieldJSONEncoder,
                    ),
                ),
                (
                    "tags",
                    taggit.managers.TaggableManager(
                        through="extras.TaggedItem", to="extras.Tag"
                    ),
                ),
                ("acs_url", models.CharField(blank=False, null=False, max_length=2000)),
                (
                    "logout_url",
                    models.CharField(blank=False, null=False, max_length=2000),
                ),
                ("force_nauth", models.BooleanField(default=False)),
                (
                    "mapping",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="saml_config",
                        to="netbox_rps_plugin.mapping",
                    ),
                ),
            ],
        ),
    ]
