"""Migration File"""
# pylint: disable=C0103

from django.db import migrations, models


class Migration(migrations.Migration):
    """Migration Class"""

    dependencies = [("netbox_rps_plugin", "0007_saml_config")]

    operations = [
        migrations.AddField(
            model_name="mapping",
            name="gzip_proxied",
            field=models.BooleanField(default=False)
        ),
        migrations.AddField(
            model_name="mapping",
            name="keepalive_requests",
            field=models.IntegerField(default=1000)
        ),
        migrations.AddField(
            model_name="mapping",
            name="keepalive_timeout",
            field=models.IntegerField(default=75)
        ),
        migrations.AddField(
            model_name="mapping",
            name="proxy_cache",
            field=models.BooleanField(default=False)
        ),
        migrations.AddField(
            model_name="mapping",
            name="proxy_read_timeout",
            field=models.IntegerField(default=60)
        ),
        migrations.AddField(
            model_name="mapping",
            name="client_max_body_size",
            field=models.IntegerField(default=1)
        ),
    ]
