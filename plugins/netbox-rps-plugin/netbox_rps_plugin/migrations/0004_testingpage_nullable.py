"""Migration File"""
# pylint: disable=C0103

from django.db import migrations, models


class Migration(migrations.Migration):
    """Migration Class"""

    dependencies = [("netbox_rps_plugin", "0003_http_header_unique_constraint")]

    operations = [
        migrations.AlterField(
            model_name="mapping",
            name="testingpage",
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
    ]
