"""Migration File"""
# pylint: disable=C0103

from django.db import migrations, models


class Migration(migrations.Migration):
    """Migration Class"""

    dependencies = [("netbox_rps_plugin", "0004_testingpage_nullable")]

    operations = [
        migrations.AlterField(
            model_name="mapping",
            name="source",
            field=models.CharField(
                null=False, blank=False, max_length=120, unique=True
            ),
        ),
    ]
