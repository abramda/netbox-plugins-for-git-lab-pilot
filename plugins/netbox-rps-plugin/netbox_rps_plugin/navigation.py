"""Navigation Menu definitions"""

from extras.plugins import PluginMenuButton, PluginMenuItem, PluginMenu
from utilities.choices import ButtonColorChoices

mapping_buttons = [
    PluginMenuButton(
        link="plugins:netbox_rps_plugin:mapping_add",
        title="Add",
        icon_class="mdi mdi-plus-thick",
        color=ButtonColorChoices.GREEN,
    ),
    PluginMenuButton(
        link="plugins:netbox_rps_plugin:mapping_add",
        title="Import",
        icon_class="mdi mdi-upload",
        color=ButtonColorChoices.CYAN,
    ),
]

mappingItem = [
    PluginMenuItem(
        link="plugins:netbox_rps_plugin:mapping_list",
        link_text="Mappings",
        buttons=mapping_buttons,
        permissions=["netbox_rps_plugin.view_mapping"],
    ),
]

menu = PluginMenu(
    label="Mappings",
    groups=(("MAPPINGS", mappingItem),),
    icon_class="mdi mdi-graph-outline",
)
