"""Model views definitions"""

from netbox.views import generic
from utilities.utils import count_related
from utilities.views import ViewTab, register_model_view
from netbox_rps_plugin import forms, tables, filtersets, models
from django.utils.translation import gettext as _


class MappingView(generic.ObjectView):
    """Mapping view definition"""

    queryset = (
        models.Mapping.objects.all()
        .prefetch_related("http_headers")
        .prefetch_related("saml_config")
    )


class MappingListView(generic.ObjectListView):
    """Mapping list view definition"""

    queryset = models.Mapping.objects.annotate(
        httpheader_count=count_related(models.HttpHeader, "mapping")
    )
    table = tables.MappingTable
    filterset = filtersets.MappingFilterSet
    filterset_form = forms.MappingFilterForm


class MappingEditView(generic.ObjectEditView):
    """Mapping edition view definition"""

    queryset = models.Mapping.objects.all()
    form = forms.MappingForm


class MappingBulkImportView(generic.BulkImportView):
    """Mapping bulk import view definition"""

    queryset = models.Mapping.objects.all()
    model_form = forms.MappingImportForm


class MappingDeleteView(generic.ObjectDeleteView):
    """Mapping delete view definition"""

    queryset = models.Mapping.objects.all()


class MappingBulkDeleteView(generic.BulkDeleteView):
    """Mapping bulk delete view definition"""

    queryset = models.Mapping.objects.all()
    filterset = filtersets.MappingFilterSet
    table = tables.MappingTable


@register_model_view(models.Mapping, "httpheader")
class MappingHttpHeadersView(generic.ObjectChildrenView):
    """Mapping HTTP Header view definition"""

    queryset = models.Mapping.objects.all().prefetch_related("http_headers")
    child_model = models.HttpHeader
    table = tables.HttpHeaderTable
    filterset = filtersets.HttpHeaderFilterSet
    template_name = "netbox_rps_plugin/httpheader/child.html"

    tab = ViewTab(
        label=_("HTTP Headers"),
        badge=lambda obj: obj.http_headers.count(),
        hide_if_empty=False,
    )

    # pylint: disable=W0613
    def get_children(self, request, parent):
        """override"""
        return parent.http_headers


@register_model_view(models.Mapping, "samlconfig")
class MappingSamlConfigView(generic.ObjectView):
    """Mapping SAML Config view definition"""

    base_template = "netbox_rps_plugin/mapping.html"
    queryset = models.Mapping.objects.all().prefetch_related("saml_config")
    template_name = "netbox_rps_plugin/saml_config.html"

    tab = ViewTab(
        label=_("SAML Configuration"),
        hide_if_empty=True,
        badge=lambda obj: 1 if hasattr(obj, "saml_config") else 0,
    )


class HttpHeaderView(generic.ObjectView):
    """HTTP Header view definition"""

    queryset = models.HttpHeader.objects.all()


class HttpHeaderListView(generic.ObjectListView):
    """HTTP Header list view definition"""

    queryset = models.HttpHeader.objects.all()
    table = tables.HttpHeaderTable
    filterset = filtersets.HttpHeaderFilterSet
    filterset_form = forms.HttpHeaderFilterForm


class HttpHeaderEditView(generic.ObjectEditView):
    """HTTP Header edition view definition"""

    queryset = models.HttpHeader.objects.all()
    form = forms.HttpHeaderForm


class HttpHeaderDeleteView(generic.ObjectDeleteView):
    """HTTP Header delete view definition"""

    queryset = models.HttpHeader.objects.all()


class HttpHeaderBulkDeleteView(generic.BulkDeleteView):
    """HTTP Header bulk delete view definition"""

    queryset = models.HttpHeader.objects.all()
    filterset = filtersets.HttpHeaderFilterSet
    table = tables.HttpHeaderTable


class SamlConfigEditView(generic.ObjectEditView):
    """HTTP SAML config edition view definition"""

    queryset = models.SamlConfig.objects.all()
    form = forms.SamlConfigForm


class SamlConfigDeleteView(generic.ObjectDeleteView):
    """HTTP SAML config delete view definition"""

    queryset = models.SamlConfig.objects.all()
