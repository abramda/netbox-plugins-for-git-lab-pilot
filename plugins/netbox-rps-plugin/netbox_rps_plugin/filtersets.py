"""Filtersets definitions"""

from django_filters import filters
from netbox.filtersets import NetBoxModelFilterSet
from django.db.models import Q
from .models import Mapping, HttpHeader


class MappingFilterSet(NetBoxModelFilterSet):
    """Mapping filterset definition class"""

    extra_protocols = filters.CharFilter(lookup_expr="icontains")

    class Meta:
        model = Mapping
        fields = (
            "id",
            "authentication",
            "source",
            "target",
            "Comment",
            "webdav",
            "extra_protocols",
            "testingpage",
            "gzip_proxied",
            "keepalive_requests",
            "keepalive_timeout",
            "proxy_cache",
            "proxy_read_timeout",
            "client_max_body_size",
            "sorry_page",
        )

    # pylint: disable=W0613
    def search(self, queryset, name, value):
        """override"""
        if not value.strip():
            return queryset
        return queryset.filter(
            Q(source__icontains=value)
            | Q(target__icontains=value)
            | Q(Comment__icontains=value)
        )


class HttpHeaderFilterSet(NetBoxModelFilterSet):
    """HTTP Header filterset definition class"""

    class Meta:
        model = HttpHeader
        fields = ("id", "name", "value", "apply_to", "mapping")

    # pylint: disable=W0613
    def search(self, queryset, name, value):
        """override"""
        if not value.strip():
            return queryset
        return queryset.filter(Q(name__icontains=value) | Q(value__icontains=value))
