"""Netbox Plugin Configuration"""

# pylint: disable=E0401
from extras.plugins import PluginConfig


class NetBoxRpsConfig(PluginConfig):
    """Netbox Plugin Configuration class"""

    name = "netbox_rps_plugin"
    verbose_name = "NetBox RPS"
    description = "A Netbox plugin to add RPS resources"
    version = "0.12.0"
    author = "Vincent Simonin"
    author_email = "vincent.simonin@ext.ec.europa.eu"
    base_url = "rps"
    default_settings = {
        "default_sorry_page": "https://sorry.ec.europa.eu/"
    }

# pylint: disable=C0103
config = NetBoxRpsConfig
