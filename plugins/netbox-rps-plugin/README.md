# RPS Plugin for Netbox
A Netbox v3.5 plugin for RPS mapping management

![Screenshot of the Netbox RPS plugin detail screen](./docs/Screenshot 2023-05-14 at 11.38.51.png)

## Quickstart
```shell
$ terraform init
$ terraform apply -var="deploy_service=true"
```

And browse [http://localhost:8000](http://localhost:8000).

* username is `username`
* and password is `password`

## Build
To build a [`netboxcommunity/netbox:v3.5-2.6.0`](https://hub.docker.com/layers/netboxcommunity/netbox/v3.5-2.6.0/images/sha256-edbb00e74a5cdb4bfeb51abbd9280cc005d1242152a67be7e2cd057e7d568fc2?context=explore) 
Docker image including the plugin pre-installed, simply run
```shell
$ docker build .
```

Refer to the community [Docker image](https://hub.docker.com/r/netboxcommunity/netbox)
for environment variable configuration.

## Tests
see [tests directory](./tests/README.md)
