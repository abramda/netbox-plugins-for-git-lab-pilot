terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

variable "deploy_administration" {
  type      = bool
  default   = false
}

variable "deploy_service" {
  type      = bool
  default   = false
}

resource "docker_network" "network" {
  name = "netbox_rps"
}


resource "docker_image" "redis" {
  name = "redis:7-alpine"
}

resource "docker_container" "redis" {
  image = docker_image.redis.image_id
  name = "redis"

  command = [
    "sh",
    "-c",  # this is to evaluate the $REDIS_PASSWORD from the env
    "redis-server --appendonly yes --requirepass \"$REDIS_PASSWORD\""  # $$ is because of Docker
  ]

  env = [
    "REDIS_PASSWORD=password"
  ]

  ports {
    internal = 6379
    external = 6379
  }

  networks_advanced {
    name = docker_network.network.name
  }
}

resource "docker_image" "postgres" {
  name = "postgres:15-alpine"
}

resource "docker_container" "postgres" {
  image = docker_image.postgres.image_id
  name = "postgres"

  env = [
    "POSTGRES_USER=username",
    "POSTGRES_PASSWORD=password",
    "POSTGRES_DB=netbox"
  ]

  ports {
    internal = 5432
    external = 5432
  }

  networks_advanced {
    name = docker_network.network.name
  }
}

resource "docker_container" "adminer" {
  count = var.deploy_administration ? 1 : 0
  image = "adminer"
  name = "adminer"


  ports {
    internal = 8080
    external = 8080
  }

  networks_advanced {
    name = docker_network.network.name
  }
}

resource "docker_image" "netbox" {
  count = var.deploy_service ? 1 : 0
  name = "netbox_rps"

  build {
    context = "."
  }
}

resource "docker_container" "netbox" {
  count = var.deploy_service ? 1 : 0
  name = "netbox"
  image = docker_image.netbox[0].image_id

  ports {
    internal = 8080
    external = 8000
  }

  networks_advanced {
    name = docker_network.network.name
  }

  env = [
    "CORS_ORIGIN_ALLOW_ALL=True",
    "DB_HOST=postgres",
    "DB_NAME=postgres",
    "DB_USER=username",
    "DB_PASSWORD=password",
    "REDIS_CACHE_DATABASE=1",
    "REDIS_CACHE_HOST=redis",
    "REDIS_CACHE_PASSWORD=password",
    "REDIS_CACHE_SSL=false",
    "REDIS_DATABASE=0",
    "REDIS_HOST=redis",
    "REDIS_PASSWORD=password",
    "REDIS_SSL=false",
    "SECRET_KEY=r8OwDznj!!dci#ParztZELKezarlkjjlazjkhat9ghmRfdu1Ysxm0AiPeDCQhKE+N_rClfWNj",
    "SKIP_SUPERUSER=false",
    "SUPERUSER_API_TOKEN=0123456789abcdef0123456789abcdef01234567",
    "SUPERUSER_EMAIL=username@global.ntt",
    "SUPERUSER_NAME=username",
    "SUPERUSER_PASSWORD=password",
  ]
}
