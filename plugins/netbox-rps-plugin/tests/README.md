# Testing the library

## Demo
```
TODO
```

## End to end
Deploy a full stack
```shell
$ terraform init
$ terraform apply -var="deploy_service=true"
```
wait `netbox` container to be ready (run `docker logs -f netbox` to watch the logs)
and prepare a python environment to execute the E2E tests suite
```shell
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -r tests/requirements.e2e.txt
$ python -m unittest discover tests/e2e
```
