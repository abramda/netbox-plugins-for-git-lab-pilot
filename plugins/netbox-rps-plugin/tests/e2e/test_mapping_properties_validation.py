"""Test case for Mapping creation"""

import unittest
import json
import os
import requests
from .base import Base


HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_KEY = os.getenv("API_KEY", "only4testingpurpose")


class TestMappingPropertiesValidation(Base):
    """Test case for Mapping properties validation domain class"""

    def test_that_gzip_proxied_is_false_by_default(self) -> None:
        """Test that gzip proxied property is false by default"""
        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["gzip_proxied"], False)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_gzip_proxied_is_boolean(self) -> None:
        """Test that gzip proxied property is a boolean"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "gzip_proxied": True,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["gzip_proxied"], True)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_proxy_cache_is_false_by_default(self) -> None:
        """Test that proxy cache property is false by default"""
        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["proxy_cache"], False)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_proxy_cache_is_boolean(self) -> None:
        """Test that proxy cache property is a boolean"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "proxy_cache": True,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["proxy_cache"], True)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_keepalive_requests_have_limit(self) -> None:
        """Test that keepalive request property is between 100 and 5000"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "keepalive_requests": 99,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"keepalive_requests":["Ensure this value is greater than or equal to 100."]}',
        )

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "keepalive_requests": 5001,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"keepalive_requests":["Ensure this value is less than or equal to 5000."]}',
        )

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "keepalive_requests": 1500,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["keepalive_requests"], 1500)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_keepalive_requests_default_value_is_1000(self) -> None:
        """Test that keepalive request property default value is 1000"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["keepalive_requests"], 1000)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_keepalive_requests_default_type_is_integer(self) -> None:
        """Test that keepalive request property type is integer"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "keepalive_requests": "toto",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content, b'{"keepalive_requests":["A valid integer is required."]}'
        )

    def test_that_keepalive_timeout_have_limit(self) -> None:
        """Test that keepalive timeout property is between 1 and 300"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "keepalive_timeout": 0,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"keepalive_timeout":["Ensure this value is greater than or equal to 1."]}',
        )

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "keepalive_timeout": 301,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"keepalive_timeout":["Ensure this value is less than or equal to 300."]}',
        )

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "keepalive_timeout": 200,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["keepalive_timeout"], 200)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_keepalive_timeout_default_value_is_75(self) -> None:
        """Test that keepalive timeout property default value is 75"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["keepalive_timeout"], 75)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_keepalive_timeout_default_type_is_integer(self) -> None:
        """Test that keepalive timeout property type is integer"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "keepalive_timeout": "toto",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content, b'{"keepalive_timeout":["A valid integer is required."]}'
        )

    def test_that_proxy_read_timeout_have_limit(self) -> None:
        """Test that proxy_read_timeout property is between 1 and 300"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "proxy_read_timeout": 0,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"proxy_read_timeout":["Ensure this value is greater than or equal to 1."]}',
        )

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "proxy_read_timeout": 301,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"proxy_read_timeout":["Ensure this value is less than or equal to 300."]}',
        )

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "proxy_read_timeout": 200,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["proxy_read_timeout"], 200)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_proxy_read_timeout_default_value_is_60(self) -> None:
        """Test that proxy_read_timeout property default value is 60"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["proxy_read_timeout"], 60)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_proxy_read_timeout_default_type_is_integer(self) -> None:
        """Test that proxy_read_timeout property type is integer"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "proxy_read_timeout": "toto",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content, b'{"proxy_read_timeout":["A valid integer is required."]}'
        )

    def test_that_client_max_body_size_have_limit(self) -> None:
        """Test that client_max_body_size property is between 1 and 255"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "client_max_body_size": 0,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"client_max_body_size":["Ensure this value is greater than or equal to 1."]}',
        )

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "client_max_body_size": 256,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"client_max_body_size":["Ensure this value is less than or equal to 255."]}',
        )

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "client_max_body_size": 200,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["client_max_body_size"], 200)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_client_max_body_size_default_value_is_60(self) -> None:
        """Test that client_max_body_size property default value is 1"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["client_max_body_size"], 1)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_client_max_body_size_default_type_is_integer(self) -> None:
        """Test that client_max_body_size property type is integer"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "proxy_read_timeout": "toto",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content, b'{"proxy_read_timeout":["A valid integer is required."]}'
        )


if __name__ == "__main__":
    unittest.main()
