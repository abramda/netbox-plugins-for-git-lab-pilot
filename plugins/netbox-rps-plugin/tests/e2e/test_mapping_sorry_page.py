"""Test case for Mapping is unique"""

import unittest
import json
import os
import requests

# from netbox_rps_plugin import NetBoxRpsConfig
from .base import Base


HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_KEY = os.getenv("API_KEY", "only4testingpurpose")


class TestMappingSorryPage(Base):
    """Test case for Mapping Sorry Page property class"""

    def test_that_mapping_sorry_page_default_value_is_set(self) -> None:
        """Test that mapping sorry page default value is set"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc8.com/api",
                "target": "http://10.10.10.10:1888/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        content = json.loads(response.content)

        self.mapping_id = content["id"]

        self.assertEqual(
            content["sorry_page"],
            "https://sorry.ec.europa.eu/",
        )

    def test_that_mapping_sorry_page_must_be_an_url(self) -> None:
        """Test that a mapping sorry page must be an URL"""
        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc8.com/api",
                "target": "http://10.10.10.10:1888/api",
                "authentication": "none",
                "testingpage": None,
                "sorry_page": "not an URL"
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc8.com/api",
                "target": "http://10.10.10.10:1888/api",
                "authentication": "none",
                "testingpage": None,
                "sorry_page": "https://my.custom.sorry.page/500.html"
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        content = json.loads(response.content)

        self.mapping_id = content["id"]

        self.assertEqual(
            content["sorry_page"],
            "https://my.custom.sorry.page/500.html",
        )


if __name__ == "__main__":
    unittest.main()
