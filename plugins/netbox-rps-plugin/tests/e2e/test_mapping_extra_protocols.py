"""Test case for Mapping protocols"""

import unittest
import json
import os
import requests
from .base import Base


HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_KEY = os.getenv("API_KEY", "only4testingpurpose")


class TestMappingProtocols(Base):
    """Test case for Mapping protocols class"""

    def test_that_mapping_protocols_default_value_is_set(self) -> None:
        """Test that mapping protocols default value is set"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

        content = json.loads(response.content)

        self.assertEqual(content["extra_protocols"], [])

    def test_that_mapping_protocols_are_not_valid(self) -> None:
        """Test that mapping protocols are not valid"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "extra_protocols": ["thisisnotavalidprotocol", "websocket", "anotherone"],
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"extra_protocols":{"0":["\\"thisisnotavalidprotocol\\" is not a valid choice."],'
            + b'"2":["\\"anotherone\\" is not a valid choice."]}}',
        )

    def test_that_mapping_protocols_are_valid(self) -> None:
        """Test that mapping protocols are not valid"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "extra_protocols": ["websocket"],
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

        content = json.loads(response.content)

        self.assertEqual(content["extra_protocols"], ["websocket"])


if __name__ == "__main__":
    unittest.main()
