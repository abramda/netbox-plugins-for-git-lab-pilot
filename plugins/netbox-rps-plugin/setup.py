from setuptools import find_packages, setup

setup(
    name='netbox_rps_plugin',
    version='0.12.0',
    description='A Netbox plugin to add RPS resources',
    install_requires=[],
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False
)
